<?php
define('COOKIE_DOMAIN', 'noithativila.vn'); // Added by W3 Total Cache
define('WP_HOME', 'https://www.noithativila.vn'); 
define('WP_SITEURL', 'https://www.noithativila.vn');
define('WP_CACHE', true); // WP-Optimize Cache
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress');
/** MySQL database username */
define( 'DB_USER', 'wordpress');
/** MySQL database password */
define( 'DB_PASSWORD', 'my_wordpress_db_password');
/** MySQL hostname */
define( 'DB_HOST', 'db:3306');
/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8');
/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '06f17818c555802db2911af91a4c2450409909ec');
define( 'SECURE_AUTH_KEY',  'f667af6a61e3c76e291aeb847941d46e9fa195e2');
define( 'LOGGED_IN_KEY',    '32415b2d814e69e686c235a26dea7c7f75d33c72');
define( 'NONCE_KEY',        '4823fbe2fdd4ec3b9255df9057179c9baaa567b4');
define( 'AUTH_SALT',        '0174ab1a15981c1e0b5e99625de34052781ec810');
define( 'SECURE_AUTH_SALT', '0c3b382f5b1980b4d168bca48675798f1ab66c2d');
define( 'LOGGED_IN_SALT',   '2e953f6f126f17defeeeda48b344a96d92d08dbb');
define( 'NONCE_SALT',       '52233baab2a94ffd6f6a88cff5de4c041ece6049');
/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';
/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
// If we're behind a proxy server and using HTTPS, we need to alert Wordpress of that fact
// see also http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}
/* That's all, stop editing! Happy publishing. */
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
define( 'FS_METHOD', 'direct' );
define('WP_POST_REVISIONS', false );
