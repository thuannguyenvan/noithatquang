<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main><!-- #main -->

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>

</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<?php wp_footer(); ?>
<!-- Load Facebook SDK for JavaScript -->
  <!-- Phần bỏ vào footer -->
<div class="call-now"  title="" data-content="<p>Chúng tôi luôn sẵn sàng Tư vấn - Giải đáp mọi thắc mắc của Quý khách.</p><p>Hoạt động 24/7 - Tất cả các ngày trong tuần, kể cả ngày lễ.</p>" data-original-title="Đường dây nóng">
    <a class="btn-call-now" href="tel:0908981645">
        <i class="icon-phone" aria-hidden="true"></i>
        <p>Tư vấn miễn phí (24/7) <strong>090 898 1645</strong></p>
    </a>
</div>
<!-- End -->    

</body>
</html>
