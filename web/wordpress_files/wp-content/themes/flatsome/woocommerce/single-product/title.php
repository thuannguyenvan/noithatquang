<?php
/**
 * Single Product title
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<h1 class="product-title product_title entry-title">
	<?php the_title(); ?>
</h1>

<?php if(get_theme_mod('product_title_divider', 1)) { ?>
	<div class="is-divider small"></div>
<?php } ?>
<?php
			$tabs = apply_filters( 'woocommerce_product_tabs', array() );

			if ( ! empty( $tabs ) ) : ?>

			<div class="woocommerce-tabs wc-tabs-wrapper container tabbed-content">
				<div class="tab-panels">
					<?php foreach ( $tabs as $key => $tab ) : ?>
						<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel entry-content active" id="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel" aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">
							<?php if ( $key != 'description' && ux_builder_is_active() ) echo flatsome_dummy_text(); ?>
							<?php if ( $key != 'description' && isset( $tab['callback'] ) ) { call_user_func( $tab['callback'], $key, $tab ); } ?>
						</div>
					<?php endforeach; ?>
				</div><!-- .tab-panels -->
			</div><!-- .tabbed-content -->

		<?php endif; ?>