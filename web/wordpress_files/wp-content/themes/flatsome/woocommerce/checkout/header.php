
<?php 
	global $woocommerce;
	function flatsome_checkout_breadcrumb_class($endpoint){
		$classes = array();
		if($endpoint == 'cart' && is_cart() ||
			$endpoint == 'checkout' && is_checkout() && !is_wc_endpoint_url('order-received') ||
			$endpoint == 'order-received' && is_wc_endpoint_url('order-received')) {
			$classes[] = 'current';
		} else{
			$classes[] = 'hide-for-small';
		}
		return implode(' ', $classes);
	}
?>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KZ9D2V3');</script>
	<!-- End Google Tag Manager -->

<div class="checkout-page-title page-title">
	<div class="page-title-inner flex-row medium-flex-wrap container">
	  <div class="flex-col flex-grow medium-text-center">
  	 	 <nav class="breadcrumbs heading-font checkout-breadcrumbs text-center h2 strong">
    	   <a href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="<?php echo flatsome_checkout_breadcrumb_class('cart'); ?>"><?php _e('Giỏ hàng', 'flatsome'); ?></a>
    	   <span class="divider hide-for-small"><?php echo get_flatsome_icon('icon-angle-right');?></span>
    	   <a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="<?php echo flatsome_checkout_breadcrumb_class('checkout') ?>"><?php _e('Chi tiết thanh toán', 'flatsome'); ?></a>
    	   <span class="divider hide-for-small"><?php echo get_flatsome_icon('icon-angle-right');?></span>
    	   <a href="#" class="no-click <?php echo flatsome_checkout_breadcrumb_class('order-received'); ?>"><?php _e('Hoàn thành đơn hàng', 'flatsome'); ?></a>
		 </nav>
	  </div><!-- .flex-left -->
	</div><!-- flex-row -->
</div><!-- .page-title -->
