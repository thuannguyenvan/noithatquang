��o]<?php exit; ?>a:6:{s:10:"last_error";s:0:"";s:10:"last_query";s:97:"SELECT wp_posts.* FROM wp_posts WHERE ID IN (596,594,592,590,588,586,584,544,542,540,538,536,531)";s:11:"last_result";a:13:{i:0;O:8:"stdClass":23:{s:2:"ID";s:3:"531";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 03:18:29";s:13:"post_date_gmt";s:19:"2019-08-22 03:18:29";s:12:"post_content";s:2133:"<!-- wp:paragraph -->
<p><em><strong>Nội Thất Lương Sơn</strong>&nbsp;xin gửi tới quý khách hàng chính sách thanh toán như sau:</em></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>I. Thanh toán trực tiếp bằng tiền mặt</strong></h3>
<!-- /wp:heading -->

<!-- wp:list -->
<ul><li>Khách hàng thanh toán cho nhân viên giao hàng công ty (đi ô tô) hoặc shipper (đi xe máy) sau khi đã nhận hàng và kiểm tra hàng hóa đầy đủ và các chứng từ kèm theo.</li><li>Giá trị thanh toán thể hiện trên hóa đơn bán hàng là giá trị “Còn lại” trên hóa đơn lấy từ giá trị “Tổng’ trừ đi phần giá trị “Tạm ứng” (nếu có).</li></ul>
<!-- /wp:list -->

<!-- wp:heading {"level":3} -->
<h3><strong>II. Thanh toán chuyển khoản:</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Đối với khách hàng xa không đến trực tiếp tại kho hoặc không có sẵn tiền mặt</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Quý khách vui lòng chuyển khoản tạm ứng hoặc thanh toán vào các tài khoản như sau:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>1.Tài khoản: Nguyễn Thị Phương</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>STK: 0021 0003 34248</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ngân hàng Vietcombank chi nhánh Hà Nội</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>2.Tài khoản: Nguyễn Thị Phương</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>STK: 4521 0000 276 391</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ngân hàng BIDV chi nhánh Thạch Thất</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>3.Tài khoản: Nguyễn Thị Phương</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>STK: 1903 0159 348 013</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ngân hàng Techcombank chi nhánh Hà Nội</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Xin trân trọng cảm ơn!</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:24:"Chính Sách Thanh Toán";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:21:"chinh-sach-thanh-toan";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-22 05:25:17";s:17:"post_modified_gmt";s:19:"2019-08-22 05:25:17";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=531";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:1;O:8:"stdClass":23:{s:2:"ID";s:3:"536";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 03:20:59";s:13:"post_date_gmt";s:19:"2019-08-22 03:20:59";s:12:"post_content";s:2278:"<!-- wp:paragraph -->
<p>Nội Thất Ivila sẽ thực hiện việc đổi/trả hàng và hoàn tiền cho khách hàng trong những trường hợp sau.</p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><li>Sản phẩm Lương Sơn giao không đúng đơn đặt hàng<br>
Hãy liên hệ với chúng tôi càng sớm càng tốt, bộ phận bán hàng của chúng tôi sẽ kiểm tra liên 1 và liên 3 (giao cho khách hàng). Trong trường hợp gửi nhầm, chúng tôi sẽ thay thế đúng mặt hàng bạn yêu cầu (khi có hàng).</li><li>Sản phẩm mua rồi nhưng không ưng ý<br>
Người mua có thể trả hàng khi không vừa ý trong vòng 48 tiếng kể từ ngày nhận hàng, Nội Thất Ivila sẽ đổi sản phẩm cho khách. Sản phẩm muốn đổi hoặc trả yêu cầu phải là sản phẩm không có dấu hiệu đã qua sử dụng và còn nguyên tem, mác, nguyên đai kiện ban đầu </li><li>Sản phẩm mua bị lỗi<br>
Quý khách vui lòng kiểm tra sản phẩm trước khi thanh toán. Trong trường hợp sản phẩm bị hư hại trong quá trình vận chuyển, quý khách vui lòng từ chối và gửi lại sản phẩm cho Nội Thất Ivila. Đồng thời thông báo cho bộ phận bán hàng, Nội Thất Ivila sẽ gửi cho quý khách mặt hàng thay thế.</li></ol>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Điều kiện đổi trả hàng</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Điều kiện về thời gian đổi trả: trong vòng 48 tiếng  kể từ khi nhận được hàng.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Điều kiện về sản phẩm:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Hàng hóa còn đầy đủ các bộ phận, không có dấu hiệu sử dụng quá nhiều hoặc hỏng hóc.<br>
Có đầy đủ các giấy tờ kèm theo (hóa đơn mua hàng, catalogue…) và các linh kiện, tặng phẩm kèm theo (nếu có).<br>
Khách hàng chịu chi phí vận chuyển cho việc đổi, trả hàng.<br>
Trường hợp không đủ các điều kiện trên thì quyền quyết định đổi, trả hàng thuộc về Nội Thất Ivila.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:39:"Chính Sách Đổi Trả, Hoàn Tiền";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:28:"chinh-sach-doi-tra-hoan-tien";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-31 10:35:46";s:17:"post_modified_gmt";s:19:"2019-08-31 10:35:46";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=536";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:2;O:8:"stdClass":23:{s:2:"ID";s:3:"538";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 03:21:58";s:13:"post_date_gmt";s:19:"2019-08-22 03:21:58";s:12:"post_content";s:1398:"<!-- wp:paragraph -->
<p>Nhằm giảm thiểu tối đa chi phí mua hàng, Nội Thất Ivila xin gửi tới quý khách hàng chính sách vận chuyển và lắp đặt như sau:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I. Chính sách vận chuyển<br>
Trong nội thành Hà Nội<br>
 Miễn phí vận chuyển với đơn hàng trên 10 triệu trong tất cả các quận nội thành Hà Nội.<br>
 Những đơn hàng nhỏ hơn, ở xa thì phí vận chuyển dựa trên phương tiện và quãng đường.<br>
 Đối với những đơn hàng vận chuyển bằng xe máy giá cước dao động từ 7.000đ -10.000đ/km<br>aĐối với những đơn hàng vận chuyển bằng xe ba gác, ô tô giá cước dao động từ 15.000đ – 18.000đ/km<br>
 Ưu đãi cho những đơn hàng lấy số lượng lớn.<br>
Ngoài nội thành Hà Nội<br>
Phí vận chuyển được tính theo km, và do sự thỏa thuận giữa nhân viên bán hàng và khách hàng.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Các tỉnh phía Nam<br>
Thành phố HCM và các tỉnh phía Nam giá bán sử dụng giá niêm yết +10%</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>II. Chính sách lắp đặt<br>
Miễn phí lắp đặt và tư vấn thiết kế (kiến trúc sư tư vấn) cho mọi đơn hàng</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:27:"Chính Sách Vận Chuyển";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:21:"chinh-sach-van-chuyen";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-31 10:35:12";s:17:"post_modified_gmt";s:19:"2019-08-31 10:35:12";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=538";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:3;O:8:"stdClass":23:{s:2:"ID";s:3:"540";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 03:22:47";s:13:"post_date_gmt";s:19:"2019-08-22 03:22:47";s:12:"post_content";s:2710:"<!-- wp:list {"ordered":true} -->
<ol><li>Thời hạn bảo hành<br>
Tất cả mặt hàng do Nội Thất Ivila cung cấp được bảo hành miễn phí 6 tháng đối với ghế văn phòng và 12 tháng với tất cả các sản phẩm nội thất khác tính từ ngày giao hàng và lắp đặt hoàn thiện.</li><li>Điều kiện bảo hành<br>
Chúng tôi sẽ tiến hành bảo hành đối với mọi trục trặc kỹ thuật đối với các lỗi thuộc về khâu sản xuất và lắp đặt gây ra.</li></ol>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Hàng hóa phải còn phiếu mua hàng từ Nội Thất Ivila (chính là phiếu bảo hành). Ngày ghi trên hóa đơn được tính là ngày giao hàng và lắp đặt hoàn thiện.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Đối với những hỏng hóc như:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Làm vỡ, làm hỏng, gây biến dạng, để lửa gây hư hại<br>
Sử dụng không đúng kỹ thuật của nhà sản xuất hoặc có dấu hiệu sửa chữa trước khi mang đến Nội Thất Ivila.<br>
Bong nẹp, bong sơn, phần vải, da, simili bọc ghế bị rách trong quá trình sử dụng.<br>
Nệm mút bị xẹp trong quá trình sử dụng.<br>
Chúng tôi vẫn có trách nhiệm sửa chữa nhưng bên mua phải thanh toán theo chi phí thực tế của vật tư linh kiện thay thế, miễn chi phí nhân công bảo hành.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Sửa chữa hoặc thay thế mới với giá ưu đãi khi hết thời hạn bảo hành.</p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><li>Thời gian và địa điểm bảo hành<br>
Trong vòng 24 giờ đồng hồ kể từ khi nhận được thông báo bằng văn bản của Quý khách hàng chúng tôi sẽ phản hồi và bảo hành trong vòng 3-5 ngày làm việc.</li></ol>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Đối với nhưng đơn hàng nhỏ lẻ số lượng từ 1- 3 chiếc bảo hành trực tiếp tại kho.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Đối với những đơn hàng lớn cồng kềnh, bảo hành tận nơi cho khách hàng.</p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><li>Hướng dẫn yêu cầu bảo hành<br>
Quý khách có thể gọi điện tới Bộ phận bảo hành theo số Hotline: 0984.316.166 hoặc gửi trực tiếp cho chúng tôi vào địa chỉ mail: baohanhluongson@gmail.com</li></ol>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Chúng tôi xin chân thành cảm ơn !</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:24:"Chính Sách Bảo Hành";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:19:"chinh-sach-bao-hanh";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-31 10:34:38";s:17:"post_modified_gmt";s:19:"2019-08-31 10:34:38";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=540";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:4;O:8:"stdClass":23:{s:2:"ID";s:3:"542";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 03:23:24";s:13:"post_date_gmt";s:19:"2019-08-22 03:23:24";s:12:"post_content";s:4892:"<!-- wp:paragraph -->
<p>Nhằm đảm bảo an toàn cho website và bảo mật thông tin cho người tiêu dùng, Nội Thất Ivila đưa ra một số chính sách bảo mật thông tin cho khách hàng cá nhân và tổ chức khi đăng nhập tại website: noithativila.vn</p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><li>Mục đích thu thập thông tin<br>
Việc thu thập thông tin qua website noithativila.vn sẽ giúp chúng tôi</li></ol>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Nắm bắt được các nguyện vọng, mong muốn của khách hàng nhằm nâng cao dịch vụ</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Giúp khách hàng cập nhật các chương trình khuyến mại, giảm giá.. do Nội Thất Ivila tổ chức sớm nhất.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Hỗ trợ khách hàng khi có khiếu nại, ý kiến một cách nhanh nhất.</p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><li>Phạm vi thu thập thông tin<br>
noithativila.vn thu thập thông tin khách hàng bao gồm</li></ol>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Tên<br>
Họ<br>
Địa chỉ email  <br>
Điện thoại<br>
Quốc gia<br>
Tỉnh Thành<br>
Địa chỉ:           <br>
Phạm vi sử dụng thông tin<br>
Chúng tôi rất coi trọng việc bảo mật thông tin khách hàng nên chúng tôi cam kết sẽ tuyệt đối không tự ý sử dụng thông tin khách hàng với mục đích không mang lại lợi ích cho khách hàng, chúng tôi cam kết không buôn bán, trao đổi thông tin bảo mật của khách hàng cho bất cứ bên thứ ba nào. Tuy nhiên, trong một số trường hợp đặc biệt sau, chúng tôi có thể chia sẻ thông tin khách một cách hợp lý khi:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Được sự đồng ý của khách hàng.<br>
Để bảo vệ quyền lợi của công ty và những đối tác của công ty: Chúng tôi chỉ đưa ra những thông tin cá nhân của khách hàng khi chắc chắn rằng những thông tin đó có thể bảo vệ được quyền lợi, tài sản của công ty chúng tôi và những đối tác liên quan. Những thông tin này sẽ được tiết lộ một cách hợp pháp theo Pháp luật Việt Nam.<br>
Theo yêu cầu của những cơ quan chính phủ khi chúng tôi thấy nó phù hợp với pháp luật Việt Nam.<br>
Trong một số trường hợp cần thiết phải cung cấp thông tin khách hàng khác, như các chương trình khuyến mãi có sự tài trợ của một bên thứ ba chẳng hạn, chúng tôi sẽ thông báo cho quý khách hàng trước khi thông tin của quý khách được chia sẻ. Qúy khách có quyền quyết định xem có đồng ý chia sẻ thông tin hoặc tham gia hay không.<br>
Thời gian lưu trữ thông tin<br>
Nội Thất Ivila sẽ lưu trữ các Thông tin cá nhân do Khách hàng cung cấp trên các hệ thống nội bộ của chúng tôi trong quá trình cung cấp dịch vụ cho Khách hàng hoặc cho đến khi hoàn thành mục đích thu thập hoặc khi Khách hàng có yêu cầu hủy các thông tin đã cung cấp.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Phương tiện và công cụ để người dùng tiếp cận và chỉnh sửa dữ liệu cá nhân của mình.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Khách hàng có thể thực hiện các quyền trên bằng cách tự truy cập vào website hoặc liên hệ với chúng tôi qua email hoặc địa chỉ liên lạc được công bố trên website của noithativila.vn</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Cam kết bảo mật thông tin cá nhân khách hàng</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Chúng tôi cam kết bảo đảm an toàn thông tin cho quý khách hàng khi đăng ký thông tin cá nhân với công ty chúng tôi. Chúng tôi cam kết không trao đổi mua bán thông tin khách hàng vì mục đích thương mại. Mọi sự chia sẻ và sử dụng thông tin khách hàng chúng tôi cam kết thực hiện theo chính sách bảo mật của công ty. Chúng tôi cam kết sẽ khiến quý khách cảm thấy tin tưởng và hài lòng về bảo mật thông tin cá nhân khi tham gia và sử dụng những dịch vụ của công ty chúng tôi.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Thông tin liên hệ<br>
Chúng tôi luôn hoan nghênh các ý kiến đóng góp, liên hệ và phản hồi thông tin từ bạn về “Chính sách bảo mật” này. Nếu bạn có những thắc mắc liên quan xin vui lòng liên hệ theo địa chỉ Email: noithativila@gmail.com</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:24:"Chính Sách Bảo Mật";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:18:"chinh-sach-bao-mat";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-31 10:33:49";s:17:"post_modified_gmt";s:19:"2019-08-31 10:33:49";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=542";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:5;O:8:"stdClass":23:{s:2:"ID";s:3:"544";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 03:23:59";s:13:"post_date_gmt";s:19:"2019-08-22 03:23:59";s:12:"post_content";s:657:"<!-- wp:paragraph -->
<p>Chính sách xử lý khiếu nại tại Nội Thất Ivila:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Tiếp nhận mọi khiếu nại của khách hàng liên quan đến việc sử dụng dịch vụ của công ty.<br>
Tất cả mọi trường hợp bảo hành, quý khách có thể liên hệ với chúng tôi để làm thủ tục bảo hành.<br>
Thời gian giải quyết khiếu nại trong thời hạn tối đa là 48 tiếng kể từ khi nhận được khiếu nại của của khách hàng. Trong trường hợp bất khả kháng 2 bên sẽ tự thương lượng.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:35:"Chính Sách Xử Lý Khiếu Nại";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:26:"chinh-sach-xu-ly-khieu-nai";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-31 10:25:11";s:17:"post_modified_gmt";s:19:"2019-08-31 10:25:11";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=544";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:6;O:8:"stdClass":23:{s:2:"ID";s:3:"584";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 04:32:59";s:13:"post_date_gmt";s:19:"2019-08-22 04:32:59";s:12:"post_content";s:3192:"<!-- wp:paragraph -->
<p>Những ngày cận Tết, mọi người bắt đầu mua sắm, trang hoàng nhà cửa để đón an lành, hạnh phúc. Ngoài những mặt hàng nhu yếu phẩm thiết yếu thì nội thất cũng được nhiều người quan tâm, chúng không chỉ là nhu cầu gắn liền với sinh hoạt mà còn là mong muốn mang lại một năm mới bình an, thịnh vượng cho mỗi gia đình.&nbsp;Sofa, bàn trà thường là bộ đôi song hành cùng nhau trong không gian phòng khách, cùng Nội Thất Nhà Đẹp xem cách chọn sofa và bàn trà cho ngày Tết ý nghĩa nhé.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Đối với người Việt Nam, phần lớn thời gian sinh hoạt, giải trí, thư giãn, tiếp khách của gia đình đều diễn ra tại phòng khách. Do vậy, phòng khách mang phong cách tối giản sẽ là sự lựa chọn tốt khi bạn muốn tạo cảm giác rộng rãi, hiện đại, sang trọng, tinh tế. Việc chọn một bộ sofa có liên kết với bàn trà về đường nét, màu sắc hay chất liệu... sẽ tạo nên bộ đôi hoàn hảo cho phòng khách.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/cach-chon-sofa-ban-tra.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Chọn sofa có chân thấp sẽ phù hợp với phong cách nội thất hiện đại, trong khi sofa dáng cao sẽ phù hợp gia đình truyền thống và có phong cách cổ điển hơn. Đặc biệt, bạn cần chú ý lựa chọn màu sắc phù hợp với màu chủ đạo của căn nhà, kích thước sofa cũng phải vừa đủ không gian phòng khách.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/cach-chon-sofa-ban-tra(1).jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Khi chon bàn trà, cách an toàn nhất đó là chọn bàn trà cùng tông màu với sofa phòng khách. Nếu là người cá tính, yêu sự phá cách bạn hãy lựa chọn tông màu đối lập giữa bàn trà và sofa, đây là cách tạo nên không gian độc đáo, lạ mắt với điểm nhấn là nội thất cho những ngày đầu năm may mắn.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/cach-chon-sofa-ban-tra(2).jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Những mẫu bàn trà đa năng, thông minh khá phù hợp với không gian hiện đại và được nhiều người tiêu dùng lựa chọn.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/cach-chon-sofa-ban-tra(3).jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><em><strong>Xem thêm các sản phẩm cho phòng khách</strong></em></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:49:"Cách chọn sofa bàn trà ngày tết ý nghĩa";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:39:"cach-chon-sofa-ban-tra-ngay-tet-y-nghia";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-22 05:22:56";s:17:"post_modified_gmt";s:19:"2019-08-22 05:22:56";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=584";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:7;O:8:"stdClass":23:{s:2:"ID";s:3:"586";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 04:33:55";s:13:"post_date_gmt";s:19:"2019-08-22 04:33:55";s:12:"post_content";s:2281:"<!-- wp:paragraph -->
<p>Theo như Franz Rivoira - Giám đốc Tiếp thị và Truyền thông của ICON, có hơn 20 năm kinh nghiệm trong lĩnh vực nội thất cao cấp và là người Ý; ông cho rằng cốt lõi của vấn đề nằm ở chỗ: Ý là một nước nghèo tài nguyên</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/dieu-gi-dua-nuoc-y-len-vi-tri-so1%20(4).jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><br>Trong lịch sử, lợi thế lớn nhất của Ý là về mặt địa lý, một quốc gia với lãnh thổ trải dài ở vị trí trung tâm Địa Trung Hải. Tuy nhiên lại không có nguồn tài nguyên dồi dào như Trung Âu. Các mỏ quặng sắt, đá quý rất nhỏ, nằm rải rác và khó khai thác. Hầu hết nguyên liệu thô đều phải nhập từ nước ngoài. Trong điều kiện như vậy, những người thợ thủ công của Ý đã mài giũa được kỹ năng chế tác đỉnh cao để tận dụng tối đa các nguyên vật liệu quý giá.<br><br>Ai cũng biết thế nào là hoàn hảo, nhưng để đạt được điều đó, cần phải có sự nỗ lực và luyện tập không ngừng. Người Ý đã trải qua hàng nghìn năm với vô vàn thế hệ, nhưng vẫn luôn khắc ghi một tôn chỉ duy nhất, như thể nó được di truyền trong dòng máu. Sự kiên trì luyện tập và không ngừng sáng tạo của người thợ thủ công đã đưa nước Ý trở thành quốc gia đứng đầu trong sản xuất nội thất cao cấp với những sản phẩm sang trọng và tinh tế.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/dieu-gi-dua-nuoc-y-len-vi-tri-so1%20(2).jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Trong sản xuất nội thất của Ý, chỉ ở mức “tốt” không bao giờ là đủ.&nbsp;<br>Dòng chữ “Made in Italy” mang ý nghĩa: Một sản phẩm cao cấp với chất lượng hoàn hảo.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:100:"Điều gì đưa nước Ý lên vị trí dẫn đầu trong lĩnh vực sản xuất nội thất";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:70:"dieu-gi-dua-nuoc-y-len-vi-tri-dan-dau-trong-linh-vuc-san-xuat-noi-that";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-22 05:22:48";s:17:"post_modified_gmt";s:19:"2019-08-22 05:22:48";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=586";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:8;O:8:"stdClass":23:{s:2:"ID";s:3:"588";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 04:34:54";s:13:"post_date_gmt";s:19:"2019-08-22 04:34:54";s:12:"post_content";s:4246:"<!-- wp:paragraph -->
<p>Một trong những điều góp phần làm nên tiếng tăm về chất lượng sofa da Ý&nbsp;của Gyform là sự kết hợp giữa công nghệ và thủ công. Để hoàn thành một bộ sofa da Gyform, luôn có sự đóng góp của những người thợ tay nghề cao và dây chuyền máy móc chuyên nghiệp. Xưởng sản xuất đặt tại Ý giúp Gyform kiểm soát hoàn toàn chất lượng của sản phẩm trong mọi công đoạn từ lúc thiết kế cho tới khi hoàn thiện và vận chuyển tới tay khách hàng.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/xuong-sx-gyform1.jpg" alt="Xưởng sản xuất sofa Gyform"/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/xuong-sx-gyform2.jpg" alt="Xưởng sản xuất sofa Gyform"/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/xuong-sx-gyform5.jpg" alt="Xưởng sản xuất sofa Gyform"/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/xuong-sx-gyform9.jpg" alt="Xưởng sản xuất sofa Gyform"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Nội thất Nhà Đẹp&nbsp;tự hào là đối tác chiến lược và là nhà&nbsp;phân phối độc quyền sofa da Gyform tại Việt Nam. Mời Quý khách tới showroom Nội thất Nhà Đẹp để chạm và cảm nhận những bộ sofa da Ý đẳng cấp.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Tham khảo:&nbsp;<strong><a href="http://nhadep.com.vn/phong-khach-sofa-da-italia" target="_blank" rel="noreferrer noopener">Các mẫu sofa da Gyform</a></strong></p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/sofa-gyform-hang-ve.jpg" alt="Sofa Gyform trưng bày tại Nội thất Nhà Đẹp 1"/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/sofa-gyform-hang-ve(1).jpg" alt="Sofa Gyform trưng bày tại Nội thất Nhà Đẹp 2"/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/sofa-gyform-hang-ve(2).jpg" alt="Sofa Gyform trưng bày tại Nội thất Nhà Đẹp 2"/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/sofa-gyform-hang-ve(3).jpg" alt="Sofa Gyform trưng bày tại Nội thất Nhà Đẹp 3"/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/sofa-gyform-hang-ve(4).jpg" alt="Sofa Gyform trưng bày tại Nội thất Nhà Đẹp 4"/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/sofa-gyform-hang-ve(5).jpg" alt="Nhãn Gyform - Made in Italy trên sản phẩm sofa Eliot"/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/sofa-gyform-hang-ve(6).jpg" alt="Đường may tỉ mỉ trên chất da cao cấp của sofa Gyform"/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/sofa-gyform-hang-ve(7).jpg" alt="Mỗi bộ sofa đều đi kèm một sổ hướng dẫn sử dụng bọc da lộn"/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/sofa-gyform-hang-ve(8).jpg" alt="Sofa da Gyform - Made in Italy"/></figure>
<!-- /wp:image -->";s:10:"post_title";s:86:"Sofa da Gyform - MADE IN ITALY chính thức có mặt tại Nội Thất Quang Việt";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:70:"sofa-da-gyform-made-in-italy-chinh-thuc-co-mat-tai-noi-that-quang-viet";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-22 05:22:43";s:17:"post_modified_gmt";s:19:"2019-08-22 05:22:43";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=588";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:9;O:8:"stdClass":23:{s:2:"ID";s:3:"590";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 04:35:33";s:13:"post_date_gmt";s:19:"2019-08-22 04:35:33";s:12:"post_content";s:4239:"<!-- wp:paragraph -->
<p>Trải qua biết bao thời kỳ cùng hàng ngàn năm hình thành và phát triển, hiện nay ngành da thuộc của Italia đang ở vị trí đỉnh cao của thế giới. Sofa da Italia chính là lựa chọn hàng đầu của những người thời thượng nhất.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Bên cạnh đó Italia còn có công nghệ tiên tiến của nước phát triển, những đội ngũ thợ lành nghề, tỉ mỉ tới từng chi tiết nhỏ nhất. Chính vì vậy đã tạo nên sản phẩm Sofa Italia luôn được làm từ da bò thật 100%. Bề mặt da luôn mềm và mịn hơn so với các loại da nhập khẩu khác, đảm bảo cả về chất lượng lẫn tính thẩm mỹ. Cùng xem 3 ưu điểm vượt trội mà một bộ sofa da Ý đem lại trong bài viết dưới đây.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>1. Chất liệu da</strong><br><br>Nói đến thành công về ngành công nghiệp da, đầu tiên phải nhắc đến sự thành công về chất lượng da. Da bò là nguyên liệu chủ yếu của sản phẩm da, chúng được chuẩn bị rất kỹ ngay từ khâu chăn nuôi đến khâu tuyển chọn. Nhờ đó thu hút được rất nhiều thương hiệu sản xuất sản phẩm về da nổi tiếng khắp thế giới trong đó có sản phẩm sofa da thật.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/3-uu-diem-vuot-troi-sofa-da.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Những con bò được chọn để khai khác da thường xuất xứ từ Italia và các nước Châu Âu lân cận. Đây là những khu vực có điều kiện khí hậu ôn hòa, đồng cỏ xanh tốt, đàn bò được chăm sóc tốt đảm bảo sức khỏe. Chính điều này đã khiến cho chất lượng da bò ở Italia chất lượng hơn so với da bò được nhập khẩu tại các nước châu Á như Trung Quốc, Ấn Độ…Công nghệ làm ghế Sofa hiện đại, được đầu tư và liên tục đổi mới, khiến cho Sofa Italia nhập khẩu dẫn đầu về chất lượng, sự bền bỉ và tuổi thọ lâu dài. Hầu như không có hiện tượng da bị rách, bong tróc, nổ da… hay ngấm nước.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/3-uu-diem-vuot-troi-sofa-da(1).jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>\</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>2. Phần khung</strong><br><br>Gỗ thông và gỗ dương, kết hợp với khung kim loại là sản phẩm chủ đạo của phần khung ghế sofa. Nhờ đó đem lại sự chắc chắn, dẻo dai, bền bỉ, chịu được trọng tải lớn, với nhiều điều kiện khác nhau. Đồng thời trọng lượng khá nhẹ, có thể dễ dàng dịch chuyển, sắp xếp.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/3-uu-diem-vuot-troi-sofa-da(2).jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>3. Sự đẳng cấp</strong><br><br>Với thiết kế đơn giản không kém phần sang trọng tạo nên một sản phẩm đẳng cấp và thời thượng. Da bò được gia công, lên màu một cách tỉ mỉ bởi người nghệ nhân lành nghề, cùng với thiết kế phù hợp. Chính điều này tạo lên sản phẩm sofa da với vẻ đẹp sang trọng, hiện đại, trở thành tâm điểm trong mọi không gian phòng khách, tạo nên sự phù hợp, đồng nhất với nhiều thiết kế nội thất khác nhau.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/3-uu-diem-vuot-troi-sofa-da(3).jpg" alt=""/></figure>
<!-- /wp:image -->";s:10:"post_title";s:49:"3 Ưu điểm vượt trội của sofa da Italia";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:38:"3-uu-diem-vuot-troi-cua-sofa-da-italia";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-22 05:22:34";s:17:"post_modified_gmt";s:19:"2019-08-22 05:22:34";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=590";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:10;O:8:"stdClass":23:{s:2:"ID";s:3:"592";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 04:36:12";s:13:"post_date_gmt";s:19:"2019-08-22 04:36:12";s:12:"post_content";s:4925:"<!-- wp:paragraph -->
<p>Opera, hội hoạ, nội thất, ẩm thực, thiên nhiên lộng lẫy, kiến trúc thăng hoa, thời trang sành điệu, không chỉ vậy, Ý còn là đất nước hội tụ những con đường mang vẻ đẹp lãng mạn. 10 con đường đẹp dưới đây chắc chắn sẽ khiến bạn muốn đến Ý ngay lập tức.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>1.&nbsp;Đường phố ở thị trấn Spello</strong><br>Spello là một thị trấn nhỏ dễ thương với các kiến trúc thời Trung cổ và Phục hưng. Những hẻm nhỏ lát đá sỏi hoặc những cây cầu bằng đá nhỏ.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Khong-gian-noi-that/Italian/duong-dep-y.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>2.&nbsp;Galuppi, Burano, Ý</strong><br>Burano là một hòn đảo nhỏ nằm trong đầm phá Venice, nơi nổi tiếng với những ngôi nhà đủ màu sắc!</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Khong-gian-noi-that/Italian/duong-dep-y1.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>3.&nbsp;Amalfi, Campania</strong><br>Amalfi là một điểm đến rất phổ biến, đặc biệt vào mùa hè nhờ có bờ biển dài và những con đường cực kỳ đẹp.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Khong-gian-noi-that/Italian/duong-dep-y2.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>4.&nbsp;Đường Monte Pertica ở Alberobello, tỉnh Bari</strong><br>Đường Monte Pertica không có gì ngoài sự thôi miên, mọi thứ đều khiến bạn không thể rời mắt được.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Khong-gian-noi-that/Italian/duong-dep-y3.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>5.&nbsp;Corso Santa Anastasia, Verona, Ý</strong><br>Trong mỗi thị trấn lớn nhỏ, bạn có thể tìm thấy những con đường có bán đủ thứ và làm bạn kinh ngạc bởi kiến trúc quá đẹp của chúng.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Khong-gian-noi-that/Italian/duong-dep-y4.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>6.&nbsp;Dolceacqua, Liguria.</strong><br>Con đường ở Dolceacqua này là một ví dụ điển hình của đường phố Ý, vì nó có sự pha trộn hoàn hảo các chi tiết mộc mạc.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Khong-gian-noi-that/Italian/duong-dep-y5.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>7. Sorno, Tuscany</strong><br>Sorno sẽ là một con đường nữa ở Ý được trang trí bằng hoa và chậu hoa. Chúng sẽ làm bạn phải mỉm cười thích thú.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Khong-gian-noi-that/Italian/duong-dep-y6.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>8. Caruggi, Liguria&nbsp;</strong><br>Đường phố ở Caruggi ẩn rất nhiều cầu thang thách thức bất kỳ du khách nào. Dù vậy dừng lo lắng vì có rất nhiều quán cà phê để bạn dừng chân khi cần.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Khong-gian-noi-that/Italian/duong-dep-y7.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>9. Bassiano, Lazio</strong><br>Bassiano là một khu phố nhỏ với những bức tường đá tạo cảm giác mát rượi.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Khong-gian-noi-that/Italian/duong-dep-y8.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>10. Molfetta, Puglia</strong><br>Con đường ở Molfetta là sự kết hợp giữa hiện đại và mộc mạc làm say đắm bất kỳ du khách nào.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Khong-gian-noi-that/Italian/duong-dep-y9.jpg" alt=""/></figure>
<!-- /wp:image -->";s:10:"post_title";s:56:"Ngất ngây với 10 con đường đẹp nhất ở Ý";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:39:"ngat-ngay-voi-10-con-duong-dep-nhat-o-y";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-22 05:22:26";s:17:"post_modified_gmt";s:19:"2019-08-22 05:22:26";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=592";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:11;O:8:"stdClass":23:{s:2:"ID";s:3:"594";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 04:36:58";s:13:"post_date_gmt";s:19:"2019-08-22 04:36:58";s:12:"post_content";s:2398:"<!-- wp:paragraph -->
<p>Không chỉ có một showroom 2 tầng rộng 1000m2 kết hợp phòng họp, quầy phục vụ cafe tại Ý, Gyform còn gây ấn tượng với những gian hàng triển lãm đầy nghệ thuật tại các sự kiện nổi tiếng thế giới về nội thất. Hãy cùng Nội thất Nhà Đẹp chiêm ngưỡng nhé.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/showroom-gyform.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/showroom-gyform1.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/showroom-gyform2.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/showroom-gyform3.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/showroom-gyform4.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/showroom-gyform5.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/showroom-gyform6.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/showroom-gyform7.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/showroom-gyform8.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/showroom-gyform9.jpg" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/showroom-gyform10.jpg" alt=""/></figure>
<!-- /wp:image -->";s:10:"post_title";s:42:"Tham quan showroom sofa da Gyform tại Ý";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:39:"tham-quan-showroom-sofa-da-gyform-tai-y";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-22 05:22:18";s:17:"post_modified_gmt";s:19:"2019-08-22 05:22:18";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=594";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:12;O:8:"stdClass":23:{s:2:"ID";s:3:"596";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-22 04:37:26";s:13:"post_date_gmt";s:19:"2019-08-22 04:37:26";s:12:"post_content";s:3640:"<!-- wp:paragraph -->
<p>Khi chúng ta nói về&nbsp;<a href="http://nhadep.com.vn/phong-khach-sofa-da-italia" target="_blank" rel="noreferrer noopener">Sofa da Ý</a>&nbsp;mọi người đều đang nghĩ rằng đó là những bộ sofa rất đắt tiền nhưng thực tế không phải vậy. Hầu hết các thương hiệu nội thất Ý luôn chọn nguyên liệu tốt nhất, cùng sự tỉ mỉ trong khâu sản xuất để tạo ra những bộ sofa da chất lượng cao nên mức giá họ bán ra hoàn toàn xứng đáng. Và 3 lý do dưới đây sẽ khiến bạn chắc chắc hơn về việc lựa chọn một bộ sofa da Ý cho không gian phòng khách của gia đình mình.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nội&nbsp;thất Nhà Đẹp&nbsp;tự hào là đối tác chiến lược và là nhà phân phối độc quyền sofa da Gyform tại Việt Nam - thương hiệu nội thất Itatlia đứng thứ 3 thế giới. Mời Quý khách tới showroom Nội thất Nhà Đẹp để chạm và cảm nhận những bộ sofa da Ý đẳng cấp.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>1. Sang trọng</strong><br><br>Không món đồ nội thất nào trong phòng có thể sánh bằng một bộ sofa Ý nếu bạn muốn phòng khách nhà bạn thêm sang trọng. Giá thành của sofa Ý có thể cao hơn nhiều so với các dòng sofa khác, nhưng điều đó hoàn toàn xứng đáng với sự tỉ mỉ trong từng chi tiết, sự chăm chút trong mọi công đoạn làm nên sản phẩm. Bạn sẽ không bao giờ hối tiếc khi đầu tư vào bộ sofa Ý bởi nó sẽ lập tức mang lại một không gian đẳng cấp</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/3-dieu-khien-ban-thich-sofa-y.jpg" alt="Sofa da Ý mang lại vẻ đẹp sang trọng, đẳng cấp cho phòng khách."/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>2. Bền bỉ với thời gian</strong><br><br>Khách hàng trên toàn thế giới tìm tới sofa Ý bởi sự bền bỉ và ít bị hao mòn so với các món đồ nội thất khác. Thông thường, một bộ sofa Ý có thể sử dụng khoảng 10 năm, nhưng nếu sản phẩm được bảo dưỡng thường xuyên, bạn có thể sử dụng sản phẩm lâu hơn thế rất nhiều.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/3-dieu-khien-ban-thich-sofa-y-1.jpg" alt="Sofa da Ý vô cùng bền vững, lớp da sẽ luôn bóng đẹp nếu thường xuyên được bảo dưỡng đúng cách."/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>3. Đa dạng về kiểu dáng với chất da tốt nhất</strong><br><br>Các thương hiệu Ý luôn đưa ra những thiết kế đa dạng cùng nhiều tính năng tích hợp trong một bộ sofa để phù hợp cho mọi không gian.Không chỉ dừng lại ở kiểu dáng da dạng, Sofa da Ý luôn lựa chọn loại da tốt nhất đem đến cảm nhận tuyệt vời cho người dùng.</p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="http://nhadep.com.vn/Uploads/images/Tu-van-noi-that/Phong-khach/3-dieu-khien-ban-thich-sofa-y-2.jpg" alt="Bạn luôn có thể lựa chọn được bộ sofa da phù hợp với phong cách nội thất của nhà mình."/></figure>
<!-- /wp:image -->";s:10:"post_title";s:46:"3 Điều khiến bạn yêu thích Sofa da Ý";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:36:"3-dieu-khien-ban-yeu-thich-sofa-da-y";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-22 05:22:12";s:17:"post_modified_gmt";s:19:"2019-08-22 05:22:12";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:30:"https://noithativila.vn/?p=596";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}s:8:"col_info";a:23:{i:0;O:8:"stdClass":13:{s:4:"name";s:2:"ID";s:7:"orgname";s:2:"ID";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:3;s:6:"length";i:20;s:9:"charsetnr";i:63;s:5:"flags";i:49699;s:4:"type";i:8;s:8:"decimals";i:0;}i:1;O:8:"stdClass":13:{s:4:"name";s:11:"post_author";s:7:"orgname";s:11:"post_author";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:1;s:6:"length";i:20;s:9:"charsetnr";i:63;s:5:"flags";i:49193;s:4:"type";i:8;s:8:"decimals";i:0;}i:2;O:8:"stdClass":13:{s:4:"name";s:9:"post_date";s:7:"orgname";s:9:"post_date";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:19;s:6:"length";i:19;s:9:"charsetnr";i:63;s:5:"flags";i:16513;s:4:"type";i:12;s:8:"decimals";i:0;}i:3;O:8:"stdClass":13:{s:4:"name";s:13:"post_date_gmt";s:7:"orgname";s:13:"post_date_gmt";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:19;s:6:"length";i:19;s:9:"charsetnr";i:63;s:5:"flags";i:129;s:4:"type";i:12;s:8:"decimals";i:0;}i:4;O:8:"stdClass":13:{s:4:"name";s:12:"post_content";s:7:"orgname";s:12:"post_content";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:4925;s:6:"length";i:4294967295;s:9:"charsetnr";i:246;s:5:"flags";i:4113;s:4:"type";i:252;s:8:"decimals";i:0;}i:5;O:8:"stdClass":13:{s:4:"name";s:10:"post_title";s:7:"orgname";s:10:"post_title";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:100;s:6:"length";i:262140;s:9:"charsetnr";i:246;s:5:"flags";i:4113;s:4:"type";i:252;s:8:"decimals";i:0;}i:6;O:8:"stdClass":13:{s:4:"name";s:12:"post_excerpt";s:7:"orgname";s:12:"post_excerpt";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:0;s:6:"length";i:262140;s:9:"charsetnr";i:246;s:5:"flags";i:4113;s:4:"type";i:252;s:8:"decimals";i:0;}i:7;O:8:"stdClass":13:{s:4:"name";s:11:"post_status";s:7:"orgname";s:11:"post_status";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:7;s:6:"length";i:80;s:9:"charsetnr";i:246;s:5:"flags";i:16385;s:4:"type";i:253;s:8:"decimals";i:0;}i:8;O:8:"stdClass":13:{s:4:"name";s:14:"comment_status";s:7:"orgname";s:14:"comment_status";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:4;s:6:"length";i:80;s:9:"charsetnr";i:246;s:5:"flags";i:1;s:4:"type";i:253;s:8:"decimals";i:0;}i:9;O:8:"stdClass":13:{s:4:"name";s:11:"ping_status";s:7:"orgname";s:11:"ping_status";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:4;s:6:"length";i:80;s:9:"charsetnr";i:246;s:5:"flags";i:1;s:4:"type";i:253;s:8:"decimals";i:0;}i:10;O:8:"stdClass":13:{s:4:"name";s:13:"post_password";s:7:"orgname";s:13:"post_password";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:0;s:6:"length";i:1020;s:9:"charsetnr";i:246;s:5:"flags";i:1;s:4:"type";i:253;s:8:"decimals";i:0;}i:11;O:8:"stdClass":13:{s:4:"name";s:9:"post_name";s:7:"orgname";s:9:"post_name";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:70;s:6:"length";i:800;s:9:"charsetnr";i:246;s:5:"flags";i:16393;s:4:"type";i:253;s:8:"decimals";i:0;}i:12;O:8:"stdClass":13:{s:4:"name";s:7:"to_ping";s:7:"orgname";s:7:"to_ping";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:0;s:6:"length";i:262140;s:9:"charsetnr";i:246;s:5:"flags";i:4113;s:4:"type";i:252;s:8:"decimals";i:0;}i:13;O:8:"stdClass":13:{s:4:"name";s:6:"pinged";s:7:"orgname";s:6:"pinged";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:0;s:6:"length";i:262140;s:9:"charsetnr";i:246;s:5:"flags";i:4113;s:4:"type";i:252;s:8:"decimals";i:0;}i:14;O:8:"stdClass":13:{s:4:"name";s:13:"post_modified";s:7:"orgname";s:13:"post_modified";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:19;s:6:"length";i:19;s:9:"charsetnr";i:63;s:5:"flags";i:129;s:4:"type";i:12;s:8:"decimals";i:0;}i:15;O:8:"stdClass":13:{s:4:"name";s:17:"post_modified_gmt";s:7:"orgname";s:17:"post_modified_gmt";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:19;s:6:"length";i:19;s:9:"charsetnr";i:63;s:5:"flags";i:129;s:4:"type";i:12;s:8:"decimals";i:0;}i:16;O:8:"stdClass":13:{s:4:"name";s:21:"post_content_filtered";s:7:"orgname";s:21:"post_content_filtered";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:0;s:6:"length";i:4294967295;s:9:"charsetnr";i:246;s:5:"flags";i:4113;s:4:"type";i:252;s:8:"decimals";i:0;}i:17;O:8:"stdClass":13:{s:4:"name";s:11:"post_parent";s:7:"orgname";s:11:"post_parent";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:1;s:6:"length";i:20;s:9:"charsetnr";i:63;s:5:"flags";i:49193;s:4:"type";i:8;s:8:"decimals";i:0;}i:18;O:8:"stdClass":13:{s:4:"name";s:4:"guid";s:7:"orgname";s:4:"guid";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:30;s:6:"length";i:1020;s:9:"charsetnr";i:246;s:5:"flags";i:1;s:4:"type";i:253;s:8:"decimals";i:0;}i:19;O:8:"stdClass":13:{s:4:"name";s:10:"menu_order";s:7:"orgname";s:10:"menu_order";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:1;s:6:"length";i:11;s:9:"charsetnr";i:63;s:5:"flags";i:32769;s:4:"type";i:3;s:8:"decimals";i:0;}i:20;O:8:"stdClass":13:{s:4:"name";s:9:"post_type";s:7:"orgname";s:9:"post_type";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:4;s:6:"length";i:80;s:9:"charsetnr";i:246;s:5:"flags";i:16393;s:4:"type";i:253;s:8:"decimals";i:0;}i:21;O:8:"stdClass":13:{s:4:"name";s:14:"post_mime_type";s:7:"orgname";s:14:"post_mime_type";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:0;s:6:"length";i:400;s:9:"charsetnr";i:246;s:5:"flags";i:1;s:4:"type";i:253;s:8:"decimals";i:0;}i:22;O:8:"stdClass":13:{s:4:"name";s:13:"comment_count";s:7:"orgname";s:13:"comment_count";s:5:"table";s:8:"wp_posts";s:8:"orgtable";s:8:"wp_posts";s:3:"def";s:0:"";s:2:"db";s:9:"wordpress";s:7:"catalog";s:3:"def";s:10:"max_length";i:1;s:6:"length";i:20;s:9:"charsetnr";i:63;s:5:"flags";i:32769;s:4:"type";i:8;s:8:"decimals";i:0;}}s:8:"num_rows";i:13;s:10:"return_val";i:13;}